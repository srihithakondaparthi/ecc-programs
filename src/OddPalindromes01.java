
public class OddPalindromes01 {

	public static void main(String[] args) {
		int num1 = 1500;
		int num2 = 2000;
		System.out.println(generateOddPalindromes(num1, num2));
	}

	public static String generateOddPalindromes(int start, int limit) {
		String nums = "";
		if (start <= 0 || limit <= 0)
			return "-1";
		else if (start >= limit)
			return "-2";
		else{
		for (int i = start; i <= limit; i++) {
			if ((isPalindrome(i) == true) && (isAllDigitsOdd(i) == true)) {
				nums = nums + i + ",";
			}
		}

		if (nums.length() == 0)
			return "-3";
		return nums.substring(0, nums.length() - 1);
	}
	}

	public static boolean isPalindrome(int num) {
		if (num == reverse(num))
			return true;
		else
			return false;

	}

	public static int reverse(int num) {
		int rev_num = 0;
		while (num > 0) {
			int r = num % 10;
			rev_num = (rev_num * 10) + r;
			num = num / 10;
		}
		return rev_num;
	}

	public static boolean isAllDigitsOdd (int num) {
		int i = 1;
		int digit = 0;
		int flag = 0;
		while (num >= i) {
			digit = num % 10;
			if(digit!=0){
			if (digit % 2 == 0) {
				flag = 1;
				break;
			}
			}
			num = num / 10;
		}
		if (flag == 1)
			return false;
		else
			return true;
	}
}
