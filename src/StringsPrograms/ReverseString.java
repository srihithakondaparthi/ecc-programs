package StringsPrograms;

import java.util.Scanner;

public class ReverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter string");
		String str = scan.nextLine();
		String result = getReverse(str);
		System.out.println("the reverse is:  " + result);

	}

	public static String doReverse(String str) {
		String reverse = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			reverse = reverse + str.charAt(i);
		}
		return reverse;

	}

	public static String getReverse(String str) {
		String reverse1 = "";
		String reverse2 = "";
		int size = str.indexOf(" ");
		String str1 = str.substring(0, size);
		for (int i = size - 1; i >= 0; i--) {
			reverse1 = doReverse(str1);
		}
		System.out.println("the reverse of first word is: " + reverse1);
		
		String str2 = str.substring(size + 1);
		int size2 = str2.length();
		for (int i = size2 - 1; i >= 0; i--) {
			reverse2 = doReverse(str2);
		}
		System.out.println("the reverse of second word is: " + reverse2);
		return reverse1 + " " + reverse2;
	}
}
