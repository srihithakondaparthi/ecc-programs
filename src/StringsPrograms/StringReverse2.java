package StringsPrograms;

import java.util.Scanner;

public class StringReverse2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter string");
		String str = scan.nextLine();
		String result = reverse(str);
		System.out.println(result);
		boolean b = isPalindome(str);
		System.out.println(b);

	}

	public static String reverse(String str) {
		String reverse = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			reverse = reverse + str.charAt(i);
		}
		return reverse;
	}

	public static boolean isPalindome(String str2) {
		if (reverse(str2).equals(str2)) {
			return true;
		} else {
			return false;
		}
	}

}
