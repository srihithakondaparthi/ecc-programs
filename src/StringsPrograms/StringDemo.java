package StringsPrograms;

public class StringDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "hello!world";
		str = str + "!!!";
		String str1 = new String("hello!india!!");
		String str2 = "amar akbar anthony";
		String firstname = str2.substring(0, str2.indexOf(' '));
		System.out.println("firstname: " + firstname);
		int s = str2.lastIndexOf(' ');
		System.out.println("second name: " + str2.substring(str2.indexOf(' ') + 1, s));
		String secondname = str2.substring(s + 1);
		System.out.println("third name: " + secondname);
		System.out.println(str);
		System.out.println(str.length());
		System.out.println(str1);
		System.out.println(str1.length());
		System.out.println("character at 0 is " + str.charAt(0));
		System.out.println("index of h is " + str.indexOf("h"));
		System.out.println(str.substring(0, 6));
		System.out.println(str.substring(5));

	}

}
