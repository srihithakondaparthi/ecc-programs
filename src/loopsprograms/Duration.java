package loopsprograms;

import java.util.Scanner;

public class Duration {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter n");
		int n = scan.nextInt();
		int r = isTotal(n);
	System.out.println("the workout durations after" + " " + n + " days is " + r);

	}

	public static int isTotal(int num) {
		int a = 1, b = 2, c = 3, d = 0;
		for (int i = 4; i <= num; i++) {
			d = a + b + c;
			a = b;
			b = c;
			c = d;
		}
		return d;
	}

}
