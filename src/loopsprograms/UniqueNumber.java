package loopsprograms;

import java.util.Scanner;

public class UniqueNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("enter the size");
		int size = scan.nextInt();
		int arr[] = new int[size];
		System.out.println("enter the elements of the array");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.nextInt();
		}
		getUnique(arr);
	}

	public static void getUnique(int arr[]) {
		int count = 0;
		System.out.println("the unique elements are");
		for (int i = 0; i < arr.length; i++) {
			count = 0;
			for (int j = 0; j < arr.length; j++) {
				if (arr[i] == arr[j])
					count++;
			}
			if (count == 1)
				System.out.println(arr[i]);
		}

	}

}
