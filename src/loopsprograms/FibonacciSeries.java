package loopsprograms;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number");
		int num = scan.nextInt();
		String r = fibonacci(num);
		System.out.println(r);

	}

	public static String fibonacci(int num) {
		int a = 0, b = 1, c = 0;
		String series = "";
		series += a;
		series = series + " " + b;
		for (int i = 3; i <= num; i++) {
			c = a + b;
			a = b;
			b = c;
			series = series + " " + b;
		}
		return series;
	}

}
