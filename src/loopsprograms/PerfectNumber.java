package loopsprograms;

import java.util.Scanner;

public class PerfectNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number");
		num = scan.nextInt();
		isPerfect(num);

	}

	public static void isPerfect(int num) {
		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0)
				sum = sum + i;

		}
		if (sum == num)
			System.out.println(num+" "+"the number is a perfect number");
		else
			System.out.println(num+" "+"not a perfect number");

	}
}
