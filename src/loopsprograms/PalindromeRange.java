package loopsprograms;

import java.util.Scanner;

public class PalindromeRange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter 1 st number");
		int n1 = scan.nextInt();
		System.out.println("enter 2 nd number");
		int n2 = scan.nextInt();
		String r = getRange(n1, n2);
		System.out.println(r);
	}
	public static int reverse(int num) {
		int rev_num = 0;
		int r;
		while (num > 0) {
			r = num % 10;
			rev_num = (rev_num * 10) + r;
			num = num / 10;
		}
		return rev_num;
	}
	public static String getRange(int n1,int n2){
		int i=0;
		String series="";
		for(i=n1;i<=n2;i++){
			if(reverse(i)==i)
				series=series+reverse(i)+" ";
		}
		return series;
		

	}
}