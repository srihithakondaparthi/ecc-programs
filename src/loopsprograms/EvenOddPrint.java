package loopsprograms;

import java.util.Scanner;

public class EvenOddPrint {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the number");
		int num = scan.nextInt();
		PrintDigitsEo(num);
	}

	public static void PrintDigitsEo(int num) {
		int i = 1;
		int count1 = 0;
		int count2 = 0;
		int digit = 0;
		while (num >= i) {
			digit = num % 10;
			num = num / 10;
			System.out.println(digit);
			if (digit % 2 == 0)
				count1++;
			else
				count2++;
		}
	System.out.println("even digit count is"+count1);
	System.out.println("odd digit count is"+count2);

	}
}
