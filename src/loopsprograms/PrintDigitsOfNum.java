package loopsprograms;

import java.util.Scanner;

public class PrintDigitsOfNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		PrintDigits(num);

	}

	public static void PrintDigits(int num) {
		int i = 1;
		int digit = 0;
		while (num>=i) {
			digit = num % 10;
			num = num / 10;
			System.out.println(digit);
		}
		
	}

}
