package loopsprograms;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		int r = reverse(num);
		System.out.println("the reverse is" + r);
		System.out.println("is the num a palindrome?" +" "+ checkPalindrome(num));
	}

	public static int reverse(int num) {
		int rev_num = 0;
		int r;
		while (num > 0) {
			r = num % 10;
			rev_num = (rev_num * 10) + r;
			num = num / 10;
		}
		return rev_num;
	}

	public static boolean checkPalindrome(int num) {
		if (num == reverse(num))
			return true;
		else
			return false;

	}

}
