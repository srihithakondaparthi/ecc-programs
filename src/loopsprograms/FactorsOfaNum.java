package loopsprograms;

import java.util.Scanner;

public class FactorsOfaNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number");
		int num = scan.nextInt();
		NumFactors(num);

	}

	public static void NumFactors(int num) {
		int i = 1;
		System.out.println("the factors are");
		while (num >= i) {
			if (num % i == 0) {
				System.out.println(i);
			}
			i++;

		}

	}
}
