package loopsprograms;

import java.util.Scanner;

public class SumOfNaturalNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the number");
		int num = scan.nextInt();
		int r = getSum(num);
		System.out.println(r);

	}

	public static int getSum(int num) {
		int i = 1;
		int sum = 0;
		while (i <= num) {
			sum = sum + i;
			i++;
		}
		return sum;
	}

}
