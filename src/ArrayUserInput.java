import java.util.Scanner;

public class ArrayUserInput {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("enter size of array");
		int size = scan.nextInt();
		int arr[] = new int[size];
		System.out.println("the elements are");
		for (int i = 0; i < size; i++) {
			arr[i] = scan.nextInt();
		}
		double r = getAvg(arr);
		System.out.println("the avg is " + r);
		System.out.println("enter the element to be searched");
		int num = scan.nextInt();
		searchElement(arr, num);
	}

	public static double getAvg(int arr[]) {
		double sum = 0;
		double avg = 0;
		for (int i = 0; i < arr.length; i++) {
			sum = sum + arr[i];
		}
		avg = sum / arr.length;
		return avg;
	}

	public static void searchElement(int arr[], int num) {
		int flag = 0;int i=0;
		for (i = 0; i < arr.length; i++) {
			if (num == arr[i])
				flag = 1;
			else
				flag = 0;
		}
		if (flag == 1)
			System.out.println("element found at "+i+"th"+" position");
		else
			System.out.println("element not found");
	}

}
