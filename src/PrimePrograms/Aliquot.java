package PrimePrograms;

import java.util.Scanner;

public class Aliquot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		int r = getAliquot(num);
		System.out.println("the aliquot number of given number is" + r);

	}

	public static int getAliquot(int num) {
		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0)
				sum = sum + i;
		}
		return sum;
	}

}
