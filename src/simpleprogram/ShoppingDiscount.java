package simpleprogram;

import java.util.Scanner;

public class ShoppingDiscount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double sp = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the price of the item");
		double cp = scan.nextDouble();
	    System.out.println("selling price is"+" "+display(cp,sp));

	}

	public static double display(double cp, double sp) {
		if ((cp > 0) && (cp <= 10000)) {
			System.out.println("you get the discount of 10%");
			double result = cp * 10 / 100;
			sp = cp - result;
			return sp;
		} else {
			if ((cp > 10000) && (cp <= 20000)) {
				System.out.println("you get the discount of 20%");
				double result = cp * 20 / 100;
				sp = cp - result;
				return sp;
			} else {
				System.out.println("you get the discount of 25%");
				double result = cp * 25 / 100;
				sp = cp - result;
				return sp;

			}

		}
	}
}
