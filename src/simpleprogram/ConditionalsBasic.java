package simpleprogram;

public class ConditionalsBasic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int age = 60;
		if (age > 90) {
			System.out.println("your age is 90");
		} else if (age > 50 && age < 90) {
			System.out.println("your age is between 50 and 90");
		} else {
			System.out.println("your age is less than 50");
		}

	}

}
