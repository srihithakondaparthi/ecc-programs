import java.util.Scanner;

public class NextHundred01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the number");
		num = scan.nextInt();
		int r = getNextMultipleOf100(num);
		System.out.println(r);

	}

	public static int getNextMultipleOf100(int num) {
		int mul=0;
		int n=num/100;
		for (int i = n; i<=(n+1) ; i++) {
			mul=i*100;
				}
	    return mul;
	}

}
